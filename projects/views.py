from django.shortcuts import render, redirect
from .models import Project
from .forms import ProjectForm
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required


@login_required
def project_list_view(request):
    model_list = Project.objects.filter(owner=request.user)
    context = {"projects": model_list}
    return render(request, "projects/projectlistview.html", context)


def user_login_redirect(request):
    login(request)
    return redirect("home")


@login_required
def project_detail_view(request, id):
    model_instance = Project.objects.get(id=id)
    context = {"detail": model_instance}
    return render(request, "projects/projectdetailview.html", context)


@login_required
def project_create_view(request):
    if request.method == "POST":
        form = ProjectForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = ProjectForm()

    context = {"form": form}

    return render(request, "projects/projectcreateview.html", context)
