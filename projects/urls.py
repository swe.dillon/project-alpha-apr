from django.urls import path
from .views import project_list_view, project_detail_view, project_create_view

urlpatterns = [
    path("create/", project_create_view, name="create_project"),
    path("<int:id>/", project_detail_view, name="show_project"),
    path("", project_list_view, name="list_projects"),
]
