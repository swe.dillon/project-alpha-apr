from django.shortcuts import render, redirect
from .forms import TaskForm, Task
from django.contrib.auth.decorators import login_required


@login_required
def task_create_view(request):
    if request.method == "POST":
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            # If you need to do something to the model before saving,
            # you can get the instance by calling
            # model_instance = form.save(commit=False)
            # Modifying the model_instance
            # and then calling model_instance.save()
            return redirect("list_projects")
    else:
        form = TaskForm()

    context = {"form": form}

    return render(request, "tasks/taskcreateview.html", context)


@login_required
def task_list_view(request):
    model_list = Task.objects.all()
    context = {"model_list": model_list}
    return render(request, "tasks/tasklistview.html", context)
