from django.urls import path
from .views import login_form, user_logout, signup_form

urlpatterns = [
    path("login/", login_form, name="login"),
    path("logout/", user_logout, name="logout"),
    path("signup/", signup_form, name="signup"),
]
